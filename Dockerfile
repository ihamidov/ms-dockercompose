FROM alpine:3.11.2
RUN apk add --no-cache openjdk8
COPY build/libs/student-0.0.1-SNAPSHOT.jar /app/app.jar
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/app.jar"]
