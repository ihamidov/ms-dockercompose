package ibrahim.student.service;

import ibrahim.student.model.Student;
import ibrahim.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class StudentService {
    //@Autowired
    private final StudentRepository studentRepository;
//    public StudentService(StudentRepository studentRepository){
//        this.studentRepository=studentRepository;
//    }

    public void createStudent(Student student){
        studentRepository.save(student);
    }

}
