package ibrahim.student.controller;

import ibrahim.student.config.AppConfig;
import ibrahim.student.model.Student;
import ibrahim.student.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StudentController {
   // @Autowired
    private final AppConfig appConfig;
    private final StudentService studentService;



    @GetMapping("/details")
    public String  getAppDeatils() {
    appConfig.getDevelopers().stream().forEach(System.out::println);
    appConfig.getNumbers().forEach((k,v)-> System.out.println(k + " " + v ));
    return appConfig.getName()+" Version: "+appConfig.getVersion();
    }


    @PostMapping
    public void createStudent(@RequestBody Student student){
       studentService.createStudent(student);
    }

}
